package com.williamhill.webviewdarkthemedemo

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.widget.Button
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e(this.javaClass.name, "onCreate called")
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val myWebView: WebView = findViewById(R.id.webView)
        setupWebView(myWebView)
        myWebView.loadUrl("https://darkmode-web.web.app")
        button = findViewById<Button>(R.id.button)
    }

    protected open fun setupWebView(webView: WebView) {
        // override to do sth with webView
    }

    protected open fun setupButtonListener(action: () -> Unit) {
        button.isVisible = true
        button.setOnClickListener { action() }
    }

    protected fun setTextForHeader(string: String) {
        findViewById<TextView>(R.id.textView).text = string
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.e(this.javaClass.name, "onConfigurationChanged called")
    }

    override fun onRestart() {
        super.onRestart()
        Log.e(this.javaClass.name, "onRestart called")
    }

    override fun onStart() {
        super.onStart()
        Log.e(this.javaClass.name, "onStart called")
    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "onResume called")
    }
}
