package com.williamhill.webviewdarkthemedemo

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log

class DefaultPrefersColorSchemeActivity : BaseActivity() {

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e(this.javaClass.name, "onCreate called")
        super.onCreate(savedInstanceState)
        setTextForHeader("Dark theme backed by DayNight theme. Auto - darkening.")
        setupButtonListener {
            startActivity(
                Intent(
                    this@DefaultPrefersColorSchemeActivity,
                    DefaultPrefersColorSchemeLightThemeActivity::class.java
                )
            )
        }
    }
}
