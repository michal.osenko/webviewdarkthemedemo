package com.williamhill.webviewdarkthemedemo

import android.content.Intent
import android.os.Bundle
import android.util.Log

class DefaultPrefersColorSchemeLightThemeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e(this.javaClass.name, "onCreate called")
        super.onCreate(savedInstanceState)
        setTextForHeader("Light theme used, dark theme backed by Force Dark theme. Auto - darkening.")
        setupButtonListener {
            startActivity(
                Intent(
                    this@DefaultPrefersColorSchemeLightThemeActivity,
                    DefaultPrefersColorSchemeLightThemeBlockForceDarkActivity::class.java
                )
            )
        }
    }
}

class DefaultPrefersColorSchemeLightThemeBlockForceDarkActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e(this.javaClass.name, "onCreate called")
        super.onCreate(savedInstanceState)
        setTextForHeader("Light theme used, force dark theme disabled in the theme.")
    }
}
